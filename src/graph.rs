use bytecounter::*;

use std::error::Error;
use std::io;
use std::path::Path;
use std::process::exit;
use std::time::Duration;

use crossterm::event::{Event, KeyCode};
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use crossterm::{event, ExecutableCommand};

use plotters::backend::DrawingBackend;
use plotters::coord;
use plotters::coord::ranged1d::ValueFormatter;
use plotters::drawing::{DrawingArea, DrawingAreaErrorKind, IntoDrawingArea};
use plotters::prelude::*;
use plotters::style::{Color, RGBColor};

use plotters_ratatui_backend::{widget_fn, RatatuiBackend};
use ratatui::layout::Rect;
use ratatui::prelude::{Alignment, Constraint, CrosstermBackend, Layout};
use ratatui::widgets::{Block, BorderType, Borders, Clear, Padding, Paragraph};
use ratatui::Terminal;

// -----------------------------------------------------------------------------

enum DisplayMode {
    Terminal,
    PNG,
}

const GREY: RGBColor = RGBColor(75, 75, 75);

struct Chart<'a> {
    data: Vec<(usize, f64)>,
    percentage_max: f64,
    title: &'a str,
    text_colour: plotters::style::RGBColor,
}

impl Chart<'_> {
    fn new(
        data: Vec<(usize, f64)>,
        percentage_max: f64,
        title: &str,
        text_colour: plotters::style::RGBColor,
    ) -> Chart {
        Chart {
            data,
            percentage_max,
            title,
            text_colour,
        }
    }

    // -------------------------------------------------------------------------

    fn contextualise<'a, DB, X, Y>(
        self: &Self,
        mut ctx: ChartContext<'a, DB, Cartesian2d<X, Y>>,
        logarithmic: bool,
        display_mode: DisplayMode,
    ) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>>
    where
        DB: DrawingBackend,
        X: Ranged<ValueType = usize> + ValueFormatter<usize> + DiscreteRanged + Clone,
        Y: Ranged<ValueType = f64> + ValueFormatter<f64>,
    {
        let bar_style = plotters::style::ShapeStyle {
            color: (if logarithmic {
                plotters::style::RED
            } else {
                plotters::style::GREEN
            })
            .mix(0.8),
            filled: true,
            stroke_width: 1,
        };

        let y_formatter = |y: &f64| {
            let n: usize = if *y < 1.0 && logarithmic {
                y.log10().abs().ceil() as usize
            } else {
                0
            };
            format!("{:.n$}", y)
        };

        let mut mesh = ctx.configure_mesh();
        let mesh = match display_mode {
            DisplayMode::Terminal => mesh.disable_mesh(),
            _ => mesh.disable_x_mesh(),
        };

        let mut y_desc = "%".to_string();
        y_desc += if logarithmic { " (log)" } else { " (lin)" };

        mesh.bold_line_style(GREY.mix(0.3))
            .light_line_style(GREY.mix(0.4))
            .y_desc(y_desc)
            .x_desc("byte index".to_string())
            .x_labels(50)
            .y_labels(self.percentage_max as usize)
            .y_label_formatter(&y_formatter)
            .axis_style(GREY)
            .axis_desc_style("".with_color(self.text_colour))
            .label_style("".with_color(self.text_colour))
            .draw()?;

        ctx.draw_series(
            Histogram::vertical(&ctx)
                .style(bar_style)
                .margin(1)
                .baseline(0.0)
                .data(
                    self.data
                        .iter()
                        .filter(|(_x, y)| *y > 0.0)
                        .map(|t| (t.0, t.1)),
                ),
        )?;
        Ok(())
    }

    // -------------------------------------------------------------------------

    pub fn draw<DB: DrawingBackend>(
        self: &Self,
        area: &DrawingArea<DB, coord::Shift>,
        logarithmic: bool,
        display_mode: DisplayMode,
    ) -> Result<(), DrawingAreaErrorKind<DB::ErrorType>> {
        let mut builder = ChartBuilder::on(&area);
        let chart = builder
            .caption(
                self.title.to_string(),
                "".into_text_style(area).with_color(self.text_colour),
            )
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5);

        if logarithmic {
            // The zero point is needed to stop the plot starting from the top and going down to
            // the value.  I don't understand this, and I don't like it turning the  0.1 Y axis
            // label in to 0.09.  So if you can improve on this situation, or at least this
            // comment, please do.  Please!
            let percentage_range = (0.0..100.0).log_scale().zero_point(-0.01);

            self.contextualise(
                chart.build_cartesian_2d(0..(255 as usize), percentage_range)?,
                true,
                display_mode,
            )?;
        } else {
            self.contextualise(
                chart.build_cartesian_2d(0..(255 as usize), 0.0..self.percentage_max)?,
                logarithmic,
                display_mode,
            )?;
        };

        area.present()
    }
}

// -----------------------------------------------------------------------------

// The use of the static TERMINAL, and concommitant unsafe code blocks below, facilitates flicker
// reduction.  It allows the Leave/Enter screen reset to be delayed until the next time tui() is
// called, so the normal screen won't be visible whilst main() is processing the next file - which
// is what caused the flicker.  Of course now there's the potential for a long unheralded delay
// when processing huge files, but you pays yer money and makes yer choice.
static mut TERMINAL: Option<Terminal<CrosstermBackend<std::io::Stdout>>> = None;

// -----------------------------------------------------------------------------

fn init_tui() -> Result<(), Box<dyn Error>> {
    unsafe {
        if TERMINAL.is_none() {
            enable_raw_mode()?;
        } else {
            io::stdout().execute(LeaveAlternateScreen)?;
        }
        io::stdout().execute(EnterAlternateScreen)?;
        TERMINAL = Some(Terminal::new(CrosstermBackend::new(io::stdout()))?);
    }
    Ok(())
}

// -----------------------------------------------------------------------------

fn tui_render_full_frame<W>(widget: W) -> Result<(), Box<dyn Error>>
where
    W: ratatui::widgets::Widget,
{
    unsafe {
        let terminal = TERMINAL.as_mut().expect("Drawing full_frame to terminal");
        terminal.draw(|frame| {
            let rect = Layout::new()
                .constraints([Constraint::Ratio(1, 1); 1])
                .split(frame.size());

            frame.render_widget(widget, rect[0]);
        })?;
    }
    Ok(())
}

// -----------------------------------------------------------------------------

fn tui_render_mid_frame<W>(widget: W, width: Option<u16>, height: Option<u16>, clear_first: bool) -> Result<(), Box<dyn Error>>
where
    W: ratatui::widgets::Widget,
{
    unsafe {
        let terminal = TERMINAL.as_mut().expect("Drawing mid_frame to terminal");
        terminal.flush()?;
        terminal.draw(|frame| {
            let f_rect = frame.size();
            let new_width = if width.is_some() {width.unwrap()} else {f_rect.width / 2};
            let new_height = if height.is_some() {height.unwrap()} else {f_rect.height / 2};
            let new_y = f_rect.height/2 - new_height/2;
            let new_x = f_rect.width/2 - new_width/2;

            let mid_rect = Rect::new(
                new_x,
                new_y,
                new_width,
                new_height,
            );
            let rect = Layout::new()
                .constraints([Constraint::Ratio(1, 1); 1])
                .split(mid_rect);

            if clear_first {
                frame.render_widget(Clear, rect[0]);
            }
            frame.render_widget(widget, rect[0]);
        })?;
    }
    Ok(())
}

// -----------------------------------------------------------------------------

pub fn tui_message(caption: &String, msg: &String) -> Result<(), Box<dyn Error>> {
    unsafe {
        if TERMINAL.is_none() {
            init_tui()?;
        }
    }

    let padded_caption = format!(" {} ", caption);
    let block = Paragraph::new(String::from(msg)).alignment(Alignment::Center).block(Block::default()
        .title(padded_caption.as_str())
        .padding(Padding::uniform(1))
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded));

    tui_render_mid_frame(block, Some(msg.len() as u16 + 10), Some(5), false)?;

    Ok(())
}

// -----------------------------------------------------------------------------

fn tui_restore_screen() -> Result<(), Box<dyn Error>> {
    io::stdout().execute(LeaveAlternateScreen)?;
    disable_raw_mode()?;

    unsafe {
        TERMINAL.as_mut().expect("showing cursor").show_cursor()?;
    }

    Ok(())
}

// -----------------------------------------------------------------------------

fn sleep(seconds: u64) {
    std::thread::sleep(Duration::from_secs(seconds));
}

// -----------------------------------------------------------------------------

pub fn tui_histogram(
    data: &Vec<(usize, f64)>,
    percentage_max: f64,
    title: &str,
    filename: &String,
    logarithmic: bool,
) -> Result<TuiHistogramReturn, Box<dyn Error>> {
    let mut logarithmic = logarithmic;

    let chart = Chart::new(data.clone(), percentage_max, title, WHITE);

    let mut direction = Direction::Forwards;
    let mut exitting = false;

    init_tui()?;

    let one_sec = Duration::from_secs(1);

    loop {
        tui_render_full_frame(widget_fn(|area| {
            chart.draw::<RatatuiBackend>(&area, logarithmic, DisplayMode::Terminal)
        }))?;
        if event::poll(one_sec)? {
            if let Event::Key(key) = event::read()? {
                match key.code {
                    KeyCode::Char('l') => logarithmic = !logarithmic,
                    KeyCode::Char('n') | KeyCode::Down | KeyCode::Right => break,
                    KeyCode::Char('p') | KeyCode::Up | KeyCode::Left => {
                        direction = Direction::Backwards;
                        break;
                    }
                    KeyCode::Char('P') => {
                        if let Err(e) = png(data, percentage_max, title, filename, logarithmic) {
                            tui_message(&"Problem writing PNG file:".to_string(), &e.to_string())?;
                            sleep(5);
                        } else {
                            tui_message(&"Success:".to_string(), &"Created PNG file".to_string())?;
                            sleep(2);
                        }
                    }
                    KeyCode::Char('q') | KeyCode::Esc => {
                        exitting = true;
                        break;
                    }
                    _ => (),
                }
            }
        }
    }

    if exitting {
        tui_restore_screen()?;
        exit(0);
    }

    Ok(TuiHistogramReturn {
        direction,
        logarithmic,
    })
}

// -----------------------------------------------------------------------------

pub fn png(
    data: &Vec<(usize, f64)>,
    percentage_max: f64,
    title: &str,
    filename: &String,
    logarithmic: bool,
) -> Result<(), Box<dyn Error>> {

    let basename = Path::new(filename.as_str()).file_name().unwrap().to_str().unwrap().to_string();
    let png =
        basename + ".bytecounter." + if logarithmic { "log" } else { "lin" } + ".png";
    let chart = Chart::new(data.clone(), percentage_max, title, BLACK);
    let area = BitMapBackend::new(&png, (1800, 900)).into_drawing_area();
    area.fill(&WHITE)?;
    chart.draw::<BitMapBackend>(&area, logarithmic, DisplayMode::PNG)?;
    area.present()?;

    Ok(())
}

// -----------------------------------------------------------------------------
//vi: sw=4:ts=4:et
