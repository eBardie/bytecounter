#[derive(Copy, Clone)]
pub enum Direction {
    Forwards = 1,
    Backwards = -1,
    Ambivalent = 0,
}

pub struct TuiHistogramReturn {
    pub direction: Direction,
    pub logarithmic: bool,
}
