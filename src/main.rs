#![feature(unix_sigpipe)]
#![feature(let_chains)]

use crate::graph::tui_message;
use bytecounter::*;

use bidir_iter::*;
use clap::{ArgAction, Parser};
use human_format::{Formatter, Scales};
use itertools::Itertools;
use memmap::MmapOptions;
use std::ascii::escape_default as escape;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs::File;
use std::option::Option;

pub mod graph;

// -----------------------------------------------------------------------------
// Commandline arguments

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// File(s) to process
    #[arg(action = ArgAction::Append)]
    files: Vec<String>,

    /// Which byte to target (0-255)
    #[arg(short, long, group = "single_byte", conflicts_with_all = ["character", "graphing"])]
    byte: Option<u8>,

    // See explict help inline
    #[arg(short, long, help="Which 8-bit/ASCII character to target
Note that whilst you can enter emoji's/UTF-8/Unicode, the results are undefined", group = "single_byte", conflicts_with_all = ["byte", "graphing"])]
    char: Option<char>,

    /// Use "human friendly" formatting e.g. '500.51MiB' instead of '524826879 bytes'
    #[arg(short = 'H', long, action)]
    human: bool,

    /// Sort stats ascending. Implies --stats
    #[arg(short, long, action, conflicts_with = "single_byte")]
    reverse: bool,

    /// Sort stats descending. Implies --stats
    #[arg(short, long, action, conflicts_with = "single_byte")]
    sort: bool,

    /// Dump stats for stats mode (default if not graphing)
    #[arg(short = 'S', long, action, default_value_ifs([("sort", "true", "true"),("reverse", "true", "true")]), conflicts_with = "single_byte")]
    stats: bool,

    // See explict help inline
    #[arg(
        short,
        long,
        help = "Plot a graph to the terminal.  Keyboard controls:
  'l' : toggle linear/logarithmic scale
  'n' : display the next file, if any ('↓' or '→' work too)
  'p' : display the previous file, if any ('↑' or '←' work too)
  'P' : emit a PNG version of the currently displayed histogram
  'q' : quit the programme (Esc works too)",
        action,
        group = "graphing",
        conflicts_with = "single_byte"
    )]
    graph: bool,

    /// Emit a PNG file (adds '.bytecounter.${log_or_lin}.png' extension to source filename)
    #[arg(
        short,
        long,
        action,
        group = "graphing",
        conflicts_with = "single_byte",
        default_value_if("log_and_lin", "true", "true")
    )]
    png: bool,

    /// Emit PNG files for both logarithmic and linear scales
    #[arg(
        short = 'P',
        long,
        action,
        group = "graphing",
        conflicts_with = "single_byte"
    )]
    log_and_lin: bool,

    /// Display graphs using a logarithmic Y axis
    #[arg(short = 'L', long, action, group = "graphing", requires_ifs = [("graph", "png")])]
    logarithmic: bool,
}

// -----------------------------------------------------------------------------

#[unix_sigpipe = "sig_dfl"] // Gracefully handle broken pipelines (e.g piping to output to `head`)
fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    let mut hf = Formatter::new();
    hf.with_separator("")
        .with_scales(Scales::Binary())
        .with_units("B");

    let mut seen_files: HashSet<&String> = HashSet::new();
    seen_files.reserve(args.files.len());

    let mut files = (&args.files[..]).bidir_iter();
    let mut logarithmic = args.logarithmic;

    if let Some(mut filename) = files.next() {
        struct Data {
            title: String,
            indexed_values: Vec<(usize, f64)>,
            percentage_max: f64,
        }

        let mut previous_direction = Direction::Ambivalent;
        let mut data_map: HashMap<&String, Data> = HashMap::new();

        loop {
            let mut direction = Direction::Ambivalent;

            let mut skip = false;

            if !seen_files.contains(filename) {
                seen_files.insert(filename);

                if args.stats || args.png {
                    println!("{}", filename);
                }
                let file = File::open(&filename)?;

                let mmap = unsafe { MmapOptions::new().map(&file) };
                if let Ok(mmap) = mmap {
                    let flen = mmap.len();
                    let formatted_count_and_flen = |count: usize| {
                        if args.human {
                            (hf.format(count as f64).replace(".00B", "B"), hf.format(flen as f64).replace(".00B", "B"))
                        } else {
                            (count.to_string(), flen.to_string() + " bytes")
                        }
                    };

                    // bytes and chars are the same in this specific context, it's just an easier
                    // way for the user to input chars
                    let mut byte = args.byte;

                    if args.byte.is_some() {
                        byte = args.byte;
                    } else if args.char.is_some() {
                        byte = Some(args.char.unwrap() as u8);
                    }

                    // Tot up all the occurences of a single sort of byte
                    if let Some(byte) = byte {
                        // bytecount::count makes use of wide register packing and fancy bit twiddling to optimize for speed
                        let count = bytecount::count(mmap.as_ref(), byte);
                        let percentage: f64 = count as f64 / (flen as f64 / 100.0);
                        let (formatted_count, formatted_flen) = formatted_count_and_flen(count);
                        println!(
                            "{} ({:.02}%) of {} are {}/0x{:02x}/{} in {}",
                            formatted_count,
                            percentage,
                            formatted_flen,
                            byte,
                            byte,
                            format!("'{}'", escape(byte)),
                            filename,
                        );
                    } else {
                        let (_, formatted_flen) = formatted_count_and_flen(0);

                        if args.graph && flen > 100_000_000 {
                            let caption = format!("Loading {formatted_flen} file:");
                            tui_message(&caption, filename)?;
                        }

                        // Stats for all 256 different bytes
                        // Can't use bytecount::count's fast fanciness since we're bucketing all
                        // all the different bytes, and 256x fast fanciness is slower than a
                        // single slow pass
                        let mut values = vec![0; 256];
                        for c in mmap.into_iter() {
                            values[*c as usize] += 1;
                        }

                        // Emit stats
                        if !(args.graph || args.png) || args.stats {
                            let mut max_count_len = 0;
                            for &count in values.iter() {
                                let (formatted_count, _) = formatted_count_and_flen(count);
                                max_count_len = usize::max(max_count_len, formatted_count.len());
                            }
                            max_count_len = usize::max(max_count_len, "count".len());

                            println!("dec  hex   esc      pct  {:>max_count_len$}", "count");

                            // Hide the Iterator type (Enumerator or IntoIterator)
                            let enumeration = values.iter().enumerate();
                            let iteration: Box<dyn Iterator<Item = (usize, &usize)>> =
                                if args.sort || args.reverse {
                                    let dir = if args.reverse { -1 } else { 1 };
                                    Box::new(enumeration.sorted_by_key(|x| dir * (*x.1 as i128)))
                                } else {
                                    Box::new(enumeration)
                                };

                            for (i, &count) in iteration {
                                let percentage: f64 = if count == 0 {
                                    0.0
                                } else {
                                    count as f64 / (flen as f64 / 100.0)
                                };
                                let (formatted_count, _) = formatted_count_and_flen(count);
                                println!(
                                    "{:>3} 0x{:02x} {:>6} {:6.02}%  {:>max_count_len$}",
                                    i,
                                    i,
                                    format!("'{}'", escape(i as u8)),
                                    percentage,
                                    formatted_count
                                );
                            }
                            println!("total: {}", formatted_flen);
                        }

                        // Graph percentage distribution
                        if args.graph || args.png {
                            let percentage_max = ((*values.iter().max().unwrap() as f64
                                / (flen as f64 / 100.0))
                                + 1.0) as f64;

                            // Marshall the data for the call to g.set_2d_data_high_resolution later on
                            let indexed_values = values
                                .iter()
                                .enumerate()
                                .map(|(i, v)| (i, *v as f64 / (flen as f64 / 100.0)))
                                .collect::<Vec<(usize, f64)>>();

                            // Stash for later use when we'll potentially be cycling in the TUI
                            data_map.insert(
                                filename,
                                Data {
                                    title: format!("{filename} - {formatted_flen}"),
                                    indexed_values,
                                    percentage_max,
                                },
                            );

                            if args.png {
                                let data = data_map.get(filename).unwrap();
                                if args.log_and_lin {
                                    graph::png(
                                        &data.indexed_values,
                                        data.percentage_max,
                                        &data.title,
                                        &filename,
                                        !logarithmic,
                                    )?;
                                }
                                graph::png(
                                    &data.indexed_values,
                                    data.percentage_max,
                                    &data.title,
                                    &filename,
                                    logarithmic,
                                )?;
                            }
                        }
                    }
                } else {
                    eprintln!("Problem mmapping {filename} : {}", mmap.err().unwrap());
                    skip = true;
                }
            }

            if args.graph {
                if !skip && let Some(data) = data_map.get(filename) {
                    let retval = graph::tui_histogram(
                        &data.indexed_values,
                        data.percentage_max,
                        &data.title,
                        &filename,
                        logarithmic,
                    )?;
                    direction = retval.direction;
                    previous_direction = direction;
                    logarithmic = retval.logarithmic;
                } else {
                    direction = previous_direction;
                }
            }

            // If we've got a direction, don't fall off either end of the iterator
            // If we're ambivalent, stop after the first run through
            match direction {
                Direction::Forwards => {
                    if let Some(next_filename) = files.next() {
                        filename = next_filename;
                    } else {
                        files.prev();
                    }
                }
                Direction::Backwards => {
                    if let Some(next_filename) = files.prev() {
                        filename = next_filename;
                    } else {
                        files.next();
                    }
                }
                Direction::Ambivalent => {
                    if let Some(next_filename) = files.next() {
                        filename = next_filename;
                    } else {
                        break;
                    }
                }
            }
        }
    }

    Ok(())
}

// -----------------------------------------------------------------------------
//vi: sw=4:ts=4:et
