[[_TOC_]]

# Bytecounter

`bytecounter` counts the number of a specific byte number (i.e one of 0-255), or gathers info on the occurrences of every  number (0-255), in a file.

If all you need is the number of bytes, you could run some variation of the following example, which uses the setandard utilities `tr` and `wc`, to display how many zero bytes are in the arbitrary file `/usr/bin/env`:

    ❯ (tr -cd '\0' | wc -c) < /usr/bin/env
    278384

How many spaces in `Cargo.lock`?

    ❯ (tr -cd ' ' | wc -c) < Cargo.lock
    1645

In the simplest case, `bytecounter` gives you some contextual information too:

    ❯ bytecounter -c' ' Cargo.lock
    1645 (4.78%) of 34406 bytes are 32/0x20/' ' in Cargo.lock

`bytecounter` can also emit totals for each byte index.  See the ["All the bytes stats" example](#all-the-bytes-stats) below.

`bytecounter` can also emit histogram plots as PNG files, with linear or logarithmic Y axis scaling.  Logarithmic scaling can help when one or two outliers forcibly bunch the rest of the data points in to obscurity.


  ![Linear scale plot pf bytes by percentage in Bytecounter's executable](doc/bytecounter.bytecounter.lin.png "Linear scale percentages of bytes in a binary file, the Bytecounter executable"){width=40%}
  ![Logarithmic scale plot of bytes by percentage in Bytecounter's executable](doc/bytecounter.bytecounter.log.png "Logarithmic scale percentages of bytes in binary file"){width=40%}


`bytecounter` can also run in [TUI](https://en.wikipedia.org/wiki/Text-based_user_interface) mode to display graphs directly in the terminal. It can toggle between linear and logarithmic scaling, or different target files, on the fly.  It can also emit a PNG version of the histogram it is currently displaying.

![Linear scale plot of bytes by percentage in Cargo.lock text file](doc/terminal_linear_text_file.png "Linear scale percentages of bytes in a text file, Bytecounter's Cargo.lock file"){width=40%}
![Logarithmic scale plot of bytes by percentage in Cargo.lock text file](doc/terminal_logarithmic_text_file.png "Logarithmic scale percentages of bytes in a text file, Bytecounter's Cargo.lock file"){width=40%}


## Commandline options

    ❯ bytecounter -h
    Usage: bytecounter [OPTIONS] [FILES]...

    Arguments:
      [FILES]...  File(s) to process

    Options:
      -b, --byte <BYTE>  Which byte to target (0-255)
      -c, --char <CHAR>  Which 8-bit/ASCII character to target
                         Note that whilst you can enter emoji's/UTF-8/Unicode, the results are undefined
      -H, --human        Use "human friendly" formatting e.g. '500.51MiB' instead of '524826879 bytes'
      -r, --reverse      Sort stats ascending. Implies --stats
      -s, --sort         Sort stats descending. Implies --stats
      -S, --stats        Dump stats for stats mode (default if not graphing)
      -g, --graph        Plot a graph to the terminal.  Keyboard controls:
                           'l' : toggle linear/logarithmic scale
                           'n' : display the next file, if any ('↓' or '→' work too)
                           'p' : display the previous file, if any ('↑' or '←' work too)
                           'P' : emit a PNG version of the currently displayed histogram
                           'q' : quit the programme (Esc works too)
      -p, --png          Emit a PNG file (adds '.bytecounter.${log_or_lin}.png' extension to source filename)
      -P, --log-and-lin  Emit PNG files for both logarithmic and linear scales
      -L, --logarithmic  Display graphs using a logarithmic Y axis
      -h, --help         Print help
      -V, --version      Print version



## Examples

### Single byte stats

If you ask for a specific byte number, e.g. `bytecounter -b0 target/release/bytecounter`:

    1549371 (23.51%) of bytecounter's 6590864 bytes are \x00 (0) bytes

You can specify any byte using `-b` and a number from `0` to `255`, or if you can type it as a character, you can use `-c`, e.g `-c ' '` to enquire how many space characters (byte 32 or 0x20) are in a file.

### All the bytes stats

Ask for stats on all of them, e.g. `bytecounter target/release/bytecounter`:

    dec hx  esc     pct   count
    000 00 \x00  23.51% 1549371
    001 01 \x01   2.15%  141458
    002 02 \x02   0.85%   55851
    …
    032 20        0.59%   38640
    033 21    !   0.07%    4774
    034 22   \"   0.07%    4588
    …
    065 41    A   0.52%   34354
    066 42    B   0.26%   17047
    067 43    C   0.18%   11938
    …
    097 61    a   1.55%  102404
    098 62    b   0.58%   37972
    099 63    c   1.53%  100666
    …
    253 fd \xfd   0.08%    4992
    254 fe \xfe   0.12%    7635
    255 ff \xff   0.88%   57730
    total: 6590864 bytes

#### Get it sorted

Sort them by frequency, either ascending (`-s`) or descending (`-r`), e.g.  `bytecounter -r target/release/bytecounter`:

    dec hx  esc     pct   count
    000 00 \x00  23.51% 1549371
    058 3a    :   2.62%  172887
    101 65    e   2.53%  166519
    036 24    $   2.17%  142991
    001 01 \x01   2.15%  141458
    …
    151 97 \x97   0.04%    2359
    150 96 \x96   0.03%    2283
    178 b2 \xb2   0.03%    2230
    174 ae \xae   0.03%    2172
    157 9d \x9d   0.03%    2171
    total: 6590864 bytes

### All the bytes' graphs

Histographs can be emitted as PNG files, or displayed in a terminal, with linear or logarithmic scales.

**Excuses**: Where the Y axis label should have 0.1 it actually has 0.09.  I'm setting a zero point to -0.01, since this stops the graphs from being messed up - by default the red bars would be drawn from the top down to their value, which is horribly confusing. And just horrible.  Anyhow, if you can explain zero points and how I could improve this aspect of the graphing, please get in contact.  Cheers.


#### Logarithmic output

When there are one or two large numbers, the rest of the data gets lost in the weeds.  Use `-L` to use logartihmic scaling for the vertical or Y axis.  This applies to both PNG and TUI graphs.

If you're not already familar with reading log scales compare the first column (byte value 0) of the first two graphs to help get your eye in.  Examining byte '\0' (0x00) which makes up *23.51%* of this particular binary file, and see where 20 is on each scale.


#### PNG file output

You get PNG output by specifying either `-p` or `-P`.

`-p` will create a linear plot by defualt, or a logarithmic one if `-L` is also specified.

`-P` with create both linear and logarithmic plots.

The files will be created with the original target file's name, with either '.lin.bytecounter.png'  '.log.bytecounter.png' appended. 

- Binary file, linear scale

  ![Linear scale plot pf bytes by percentage in Bytecounter's executable](doc/bytecounter.bytecounter.lin.png "Linear scale percentages of bytes in a binary file, the Bytecounter executable")

- Binary file, logarithmic scale

  ![Logarithmic scale plot of bytes by percentage in Bytecounter's executable](doc/bytecounter.bytecounter.log.png "Logarithmic scale percentages of bytes in binary file")

- Text file, linear scale

  ![Linear scale plot of bytes by percentage in Cargo.lock text file](doc/Cargo.lock.bytecounter.lin.png "Linear scale percentages of bytes in a text file, Bytecounter's Cargo.lock file")

- Text file, logarithmic scale

  ![Logarithmic scale plot of bytes by percentage in Cargo.lock text file](doc/Cargo.lock.bytecounter.log.png "Logarithmic scale percentages of bytes in a text file, Bytecounter's Cargo.lock file")


#### Terminal display

In TUI mode (`-g`) you can toggle linear/logarithmic scaling by pressing 'l'.

To quit the programme back to the terminal shell, press 'q' or the Escape key.

If you started `bytecounter` with multiple target files, you can cycle between them in TUI mode by pressing  `p` (previous) and `n` (next) or using the arrow keys (`←` and `→`, or `↑` and `↓`).

Pressing `P` (note it's the capital letter) will emit a PNG version of the currently displayed histogram.

**Note**: My terminal has a blurred background image, which shows up in these screenshots.

- Binary file, linear scale

  ![Linear scale plot pf bytes by percentage in Bytecounter's executable](doc/terminal_linear_binary_file.png "Linear scale percentages of bytes in a binary file, the Bytecounter executable")

- Binary file, logarithmic scale

  ![Logarithmic scale plot of bytes by percentage in Bytecounter's executable](doc/terminal_logarithmic_binary_file.png "Logarithmic scale percentages of bytes in binary file")

- Text file, linear scale

  ![Linear scale plot of bytes by percentage in Cargo.lock text file](doc/terminal_linear_text_file.png "Linear scale percentages of bytes in a text file, Bytecounter's Cargo.lock file")

- Text file, logarithmic scale

  ![Logarithmic scale plot of bytes by percentage in Cargo.lock text file](doc/terminal_logarithmic_text_file.png "Logarithmic scale percentages of bytes in a text file, Bytecounter's Cargo.lock file")


