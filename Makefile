release:
	RUSTFLAGS="-C target-cpu=native" cargo build --release --features runtime-dispatch-simd --features generic-simd
debug:
	RUSTFLAGS="-C target-cpu=native" cargo build           --features runtime-dispatch-simd --features generic-simd
